FROM registry.gitlab.com/cloudmonitor/images/laravel
ARG BRANCH=main
ARG NPMRC=
USER root
COPY . .
RUN sh /install
